(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[30],{

/***/ "./node_modules/@theia/timeline/lib/browser/timeline-context-key-service.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/@theia/timeline/lib/browser/timeline-context-key-service.js ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/********************************************************************************
 * Copyright (C) 2020 RedHat and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimelineContextKeyService = void 0;
var inversify_1 = __webpack_require__(/*! inversify */ "./node_modules/inversify/lib/inversify.js");
var context_key_service_1 = __webpack_require__(/*! @theia/core/lib/browser/context-key-service */ "./node_modules/@theia/core/lib/browser/context-key-service.js");
var TimelineContextKeyService = /** @class */ (function () {
    function TimelineContextKeyService() {
    }
    Object.defineProperty(TimelineContextKeyService.prototype, "timelineItem", {
        get: function () {
            return this._timelineItem;
        },
        enumerable: false,
        configurable: true
    });
    TimelineContextKeyService.prototype.init = function () {
        this._timelineItem = this.contextKeyService.createKey('timelineItem', undefined);
    };
    __decorate([
        inversify_1.inject(context_key_service_1.ContextKeyService),
        __metadata("design:type", context_key_service_1.ContextKeyService)
    ], TimelineContextKeyService.prototype, "contextKeyService", void 0);
    __decorate([
        inversify_1.postConstruct(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TimelineContextKeyService.prototype, "init", null);
    TimelineContextKeyService = __decorate([
        inversify_1.injectable()
    ], TimelineContextKeyService);
    return TimelineContextKeyService;
}());
exports.TimelineContextKeyService = TimelineContextKeyService;


/***/ }),

/***/ "./node_modules/@theia/timeline/lib/browser/timeline-contribution.js":
/*!***************************************************************************!*\
  !*** ./node_modules/@theia/timeline/lib/browser/timeline-contribution.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/********************************************************************************
 * Copyright (C) 2020 RedHat and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimelineContribution = void 0;
var inversify_1 = __webpack_require__(/*! inversify */ "./node_modules/inversify/lib/inversify.js");
var browser_1 = __webpack_require__(/*! @theia/core/lib/browser */ "./node_modules/@theia/core/lib/browser/index.js");
var browser_2 = __webpack_require__(/*! @theia/navigator/lib/browser */ "./node_modules/@theia/navigator/lib/browser/index.js");
var timeline_widget_1 = __webpack_require__(/*! ./timeline-widget */ "./node_modules/@theia/timeline/lib/browser/timeline-widget.js");
var timeline_service_1 = __webpack_require__(/*! ./timeline-service */ "./node_modules/@theia/timeline/lib/browser/timeline-service.js");
var common_1 = __webpack_require__(/*! @theia/core/lib/common */ "./node_modules/@theia/core/lib/common/index.js");
var tab_bar_toolbar_1 = __webpack_require__(/*! @theia/core/lib/browser/shell/tab-bar-toolbar */ "./node_modules/@theia/core/lib/browser/shell/tab-bar-toolbar.js");
var algorithm_1 = __webpack_require__(/*! @phosphor/algorithm */ "./node_modules/@phosphor/algorithm/lib/index.js");
var TimelineContribution = /** @class */ (function () {
    function TimelineContribution() {
        this.toolbarItem = {
            id: 'timeline-refresh-toolbar-item',
            command: 'timeline-refresh',
            tooltip: 'Refresh',
            icon: 'fa fa-refresh'
        };
    }
    TimelineContribution_1 = TimelineContribution;
    TimelineContribution.prototype.registerToolbarItems = function (registry) {
        registry.registerItem(this.toolbarItem);
    };
    TimelineContribution.prototype.registerCommands = function (commands) {
        var _this = this;
        var attachTimeline = function (explorer) { return __awaiter(_this, void 0, void 0, function () {
            var timeline;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.widgetManager.getOrCreateWidget(timeline_widget_1.TimelineWidget.ID)];
                    case 1:
                        timeline = _a.sent();
                        if (explorer instanceof browser_1.ViewContainer && explorer.getTrackableWidgets().indexOf(timeline) === -1) {
                            explorer.addWidget(timeline, { initiallyCollapsed: true });
                        }
                        return [2 /*return*/];
                }
            });
        }); };
        this.widgetManager.onWillCreateWidget(function (event) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (event.widget.id === browser_2.EXPLORER_VIEW_CONTAINER_ID && this.timelineService.getSources().length > 0) {
                    event.waitUntil(attachTimeline(event.widget));
                }
                return [2 /*return*/];
            });
        }); });
        this.timelineService.onDidChangeProviders(function (event) { return __awaiter(_this, void 0, void 0, function () {
            var explorer, timeline;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.widgetManager.getWidget(browser_2.EXPLORER_VIEW_CONTAINER_ID)];
                    case 1:
                        explorer = _a.sent();
                        if (!(explorer && event.added && event.added.length > 0)) return [3 /*break*/, 2];
                        attachTimeline(explorer);
                        return [3 /*break*/, 4];
                    case 2:
                        if (!(event.removed && this.timelineService.getSources().length === 0)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.widgetManager.getWidget(timeline_widget_1.TimelineWidget.ID)];
                    case 3:
                        timeline = _a.sent();
                        if (timeline) {
                            timeline.close();
                        }
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        }); });
        commands.registerCommand(TimelineContribution_1.LOAD_MORE_COMMAND, {
            execute: function () { return __awaiter(_this, void 0, void 0, function () {
                var widget, uri, timeline;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            widget = algorithm_1.toArray(this.shell.mainPanel.widgets()).find(function (w) { return browser_1.Navigatable.is(w) && w.isVisible && !w.isHidden; });
                            if (!browser_1.Navigatable.is(widget)) return [3 /*break*/, 2];
                            uri = widget.getResourceUri();
                            return [4 /*yield*/, this.widgetManager.getWidget(timeline_widget_1.TimelineWidget.ID)];
                        case 1:
                            timeline = _a.sent();
                            if (uri && timeline) {
                                timeline.loadTimeline(uri, false);
                            }
                            _a.label = 2;
                        case 2: return [2 /*return*/];
                    }
                });
            }); }
        });
        commands.registerCommand({ id: this.toolbarItem.command }, {
            execute: function (widget) { return _this.checkWidget(widget, function () { return __awaiter(_this, void 0, void 0, function () {
                var timeline;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.widgetManager.getWidget(timeline_widget_1.TimelineWidget.ID)];
                        case 1:
                            timeline = _a.sent();
                            if (timeline) {
                                timeline.update();
                            }
                            return [2 /*return*/];
                    }
                });
            }); }); },
            isEnabled: function (widget) { return _this.checkWidget(widget, function () { return true; }); },
            isVisible: function (widget) { return _this.checkWidget(widget, function () { return true; }); }
        });
    };
    TimelineContribution.prototype.checkWidget = function (widget, cb) {
        if (widget instanceof timeline_widget_1.TimelineWidget && widget.id === timeline_widget_1.TimelineWidget.ID) {
            return cb();
        }
        return false;
    };
    var TimelineContribution_1;
    TimelineContribution.LOAD_MORE_COMMAND = {
        id: 'timeline-load-more'
    };
    __decorate([
        inversify_1.inject(browser_1.WidgetManager),
        __metadata("design:type", browser_1.WidgetManager)
    ], TimelineContribution.prototype, "widgetManager", void 0);
    __decorate([
        inversify_1.inject(timeline_service_1.TimelineService),
        __metadata("design:type", timeline_service_1.TimelineService)
    ], TimelineContribution.prototype, "timelineService", void 0);
    __decorate([
        inversify_1.inject(common_1.CommandRegistry),
        __metadata("design:type", common_1.CommandRegistry)
    ], TimelineContribution.prototype, "commandRegistry", void 0);
    __decorate([
        inversify_1.inject(tab_bar_toolbar_1.TabBarToolbarRegistry),
        __metadata("design:type", tab_bar_toolbar_1.TabBarToolbarRegistry)
    ], TimelineContribution.prototype, "tabBarToolbar", void 0);
    __decorate([
        inversify_1.inject(browser_1.ApplicationShell),
        __metadata("design:type", browser_1.ApplicationShell)
    ], TimelineContribution.prototype, "shell", void 0);
    TimelineContribution = TimelineContribution_1 = __decorate([
        inversify_1.injectable()
    ], TimelineContribution);
    return TimelineContribution;
}());
exports.TimelineContribution = TimelineContribution;


/***/ }),

/***/ "./node_modules/@theia/timeline/lib/browser/timeline-empty-widget.js":
/*!***************************************************************************!*\
  !*** ./node_modules/@theia/timeline/lib/browser/timeline-empty-widget.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/********************************************************************************
 * Copyright (C) 2020 RedHat and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimelineEmptyWidget = void 0;
var inversify_1 = __webpack_require__(/*! inversify */ "./node_modules/inversify/lib/inversify.js");
var browser_1 = __webpack_require__(/*! @theia/core/lib/browser */ "./node_modules/@theia/core/lib/browser/index.js");
var alert_message_1 = __webpack_require__(/*! @theia/core/lib/browser/widgets/alert-message */ "./node_modules/@theia/core/lib/browser/widgets/alert-message.js");
var React = __webpack_require__(/*! react */ "./node_modules/react/index.js");
var TimelineEmptyWidget = /** @class */ (function (_super) {
    __extends(TimelineEmptyWidget, _super);
    function TimelineEmptyWidget() {
        var _this = _super.call(this) || this;
        _this.addClass('theia-timeline-empty');
        _this.id = TimelineEmptyWidget_1.ID;
        return _this;
    }
    TimelineEmptyWidget_1 = TimelineEmptyWidget;
    TimelineEmptyWidget.prototype.render = function () {
        return React.createElement(alert_message_1.AlertMessage, { type: 'WARNING', header: 'The active editor cannot provide timeline information.' });
    };
    var TimelineEmptyWidget_1;
    TimelineEmptyWidget.ID = 'timeline-empty-widget';
    TimelineEmptyWidget = TimelineEmptyWidget_1 = __decorate([
        inversify_1.injectable(),
        __metadata("design:paramtypes", [])
    ], TimelineEmptyWidget);
    return TimelineEmptyWidget;
}(browser_1.ReactWidget));
exports.TimelineEmptyWidget = TimelineEmptyWidget;


/***/ }),

/***/ "./node_modules/@theia/timeline/lib/browser/timeline-service.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@theia/timeline/lib/browser/timeline-service.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/********************************************************************************
 * Copyright (C) 2020 RedHat and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimelineAggregate = exports.TimelineService = void 0;
var inversify_1 = __webpack_require__(/*! inversify */ "./node_modules/inversify/lib/inversify.js");
var common_1 = __webpack_require__(/*! @theia/core/lib/common */ "./node_modules/@theia/core/lib/common/index.js");
var TimelineService = /** @class */ (function () {
    function TimelineService() {
        this.providers = new Map();
        this.onDidChangeProvidersEmitter = new common_1.Emitter();
        this.onDidChangeProviders = this.onDidChangeProvidersEmitter.event;
        this.onDidChangeTimelineEmitter = new common_1.Emitter();
        this.onDidChangeTimeline = this.onDidChangeTimelineEmitter.event;
    }
    TimelineService.prototype.registerTimelineProvider = function (provider) {
        var _this = this;
        var id = provider.id;
        this.providers.set(id, provider);
        if (provider.onDidChange) {
            provider.onDidChange(function (e) { return _this.onDidChangeTimelineEmitter.fire(e); });
        }
        this.onDidChangeProvidersEmitter.fire({ added: [id] });
        return common_1.Disposable.create(function () { return _this.unregisterTimelineProvider(id); });
    };
    TimelineService.prototype.unregisterTimelineProvider = function (id) {
        var provider = this.providers.get(id);
        if (provider) {
            provider.dispose();
            this.providers.delete(id);
            this.onDidChangeProvidersEmitter.fire({ removed: [id] });
        }
    };
    TimelineService.prototype.getSources = function () {
        return __spread(this.providers.values()).map(function (p) { return ({ id: p.id, label: p.label }); });
    };
    TimelineService.prototype.getSchemas = function () {
        var result = [];
        Array.from(this.providers.values()).forEach(function (provider) {
            var scheme = provider.scheme;
            if (typeof scheme === 'string') {
                result.push(scheme);
            }
            else {
                scheme.forEach(function (s) { return result.push(s); });
            }
        });
        return result;
    };
    TimelineService.prototype.getTimeline = function (id, uri, options, internalOptions) {
        var provider = this.providers.get(id);
        if (!provider) {
            return Promise.resolve(undefined);
        }
        if (typeof provider.scheme === 'string') {
            if (provider.scheme !== '*' && provider.scheme !== uri.scheme) {
                return Promise.resolve(undefined);
            }
        }
        return provider.provideTimeline(uri, options, internalOptions)
            .then(function (result) {
            if (!result) {
                return undefined;
            }
            result.items = result.items.map(function (item) { return (__assign(__assign({}, item), { source: provider.id })); });
            return result;
        });
    };
    TimelineService = __decorate([
        inversify_1.injectable()
    ], TimelineService);
    return TimelineService;
}());
exports.TimelineService = TimelineService;
var TimelineAggregate = /** @class */ (function () {
    function TimelineAggregate(timeline) {
        var _a;
        this.source = timeline.source;
        this.items = timeline.items;
        this._cursor = (_a = timeline.paging) === null || _a === void 0 ? void 0 : _a.cursor;
    }
    Object.defineProperty(TimelineAggregate.prototype, "cursor", {
        get: function () {
            return this._cursor;
        },
        set: function (cursor) {
            this._cursor = cursor;
        },
        enumerable: false,
        configurable: true
    });
    TimelineAggregate.prototype.add = function (items) {
        var _a;
        (_a = this.items).push.apply(_a, __spread(items));
        this.items.sort(function (a, b) { return b.timestamp - a.timestamp; });
    };
    return TimelineAggregate;
}());
exports.TimelineAggregate = TimelineAggregate;


/***/ }),

/***/ "./node_modules/@theia/timeline/lib/browser/timeline-tree-model.js":
/*!*************************************************************************!*\
  !*** ./node_modules/@theia/timeline/lib/browser/timeline-tree-model.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/********************************************************************************
 * Copyright (C) 2020 RedHat and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimelineTreeModel = void 0;
var inversify_1 = __webpack_require__(/*! inversify */ "./node_modules/inversify/lib/inversify.js");
var tree_1 = __webpack_require__(/*! @theia/core/lib/browser/tree */ "./node_modules/@theia/core/lib/browser/tree/index.js");
var timeline_contribution_1 = __webpack_require__(/*! ./timeline-contribution */ "./node_modules/@theia/timeline/lib/browser/timeline-contribution.js");
var TimelineTreeModel = /** @class */ (function (_super) {
    __extends(TimelineTreeModel, _super);
    function TimelineTreeModel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TimelineTreeModel.prototype.updateTree = function (items, hasMoreItems) {
        var root = {
            id: 'timeline-tree-root',
            parent: undefined,
            visible: false,
            children: []
        };
        var children = items.map(function (item) {
            return ({
                timelineItem: item,
                id: item.id ? item.id : item.timestamp.toString(),
                parent: root,
                detail: item.detail,
                selected: false,
                visible: true
            });
        });
        var loadMore;
        if (hasMoreItems) {
            var loadMoreNode = { label: 'Load-more', timestamp: 0, handle: '', uri: '', source: '' };
            loadMoreNode.command = timeline_contribution_1.TimelineContribution.LOAD_MORE_COMMAND;
            loadMore = {
                timelineItem: loadMoreNode,
                id: 'load-more',
                parent: root,
                selected: true
            };
            children.push(loadMore);
        }
        root.children = children;
        this.root = root;
        if (loadMore) {
            this.selectionService.addSelection(loadMore);
        }
    };
    TimelineTreeModel = __decorate([
        inversify_1.injectable()
    ], TimelineTreeModel);
    return TimelineTreeModel;
}(tree_1.TreeModelImpl));
exports.TimelineTreeModel = TimelineTreeModel;


/***/ }),

/***/ "./node_modules/@theia/timeline/lib/browser/timeline-tree-widget.js":
/*!**************************************************************************!*\
  !*** ./node_modules/@theia/timeline/lib/browser/timeline-tree-widget.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/********************************************************************************
 * Copyright (C) 2020 RedHat and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimelineItemNode = exports.TimelineTreeWidget = exports.TIMELINE_ITEM_CONTEXT_MENU = void 0;
var inversify_1 = __webpack_require__(/*! inversify */ "./node_modules/inversify/lib/inversify.js");
var common_1 = __webpack_require__(/*! @theia/core/lib/common */ "./node_modules/@theia/core/lib/common/index.js");
var tree_1 = __webpack_require__(/*! @theia/core/lib/browser/tree */ "./node_modules/@theia/core/lib/browser/tree/index.js");
var browser_1 = __webpack_require__(/*! @theia/core/lib/browser */ "./node_modules/@theia/core/lib/browser/index.js");
var timeline_tree_model_1 = __webpack_require__(/*! ./timeline-tree-model */ "./node_modules/@theia/timeline/lib/browser/timeline-tree-model.js");
var timeline_service_1 = __webpack_require__(/*! ./timeline-service */ "./node_modules/@theia/timeline/lib/browser/timeline-service.js");
var timeline_context_key_service_1 = __webpack_require__(/*! ./timeline-context-key-service */ "./node_modules/@theia/timeline/lib/browser/timeline-context-key-service.js");
var React = __webpack_require__(/*! react */ "./node_modules/react/index.js");
exports.TIMELINE_ITEM_CONTEXT_MENU = ['timeline-item-context-menu'];
var TimelineTreeWidget = /** @class */ (function (_super) {
    __extends(TimelineTreeWidget, _super);
    function TimelineTreeWidget(props, timelineService, model, contextMenuRenderer, commandRegistry) {
        var _this = _super.call(this, props, model, contextMenuRenderer) || this;
        _this.props = props;
        _this.timelineService = timelineService;
        _this.model = model;
        _this.contextMenuRenderer = contextMenuRenderer;
        _this.commandRegistry = commandRegistry;
        _this.id = TimelineTreeWidget_1.ID;
        _this.addClass('timeline-outer-container');
        return _this;
    }
    TimelineTreeWidget_1 = TimelineTreeWidget;
    TimelineTreeWidget.prototype.renderNode = function (node, props) {
        var attributes = this.createNodeAttributes(node, props);
        var content = React.createElement(TimelineItemNode, { timelineItem: node.timelineItem, commandRegistry: this.commandRegistry, contextKeys: this.contextKeys, contextMenuRenderer: this.contextMenuRenderer });
        return React.createElement('div', attributes, content);
    };
    TimelineTreeWidget.prototype.handleEnter = function (event) {
        var _a;
        var node = this.model.selectedNodes[0];
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        var command = node.timelineItem.command;
        if (command) {
            (_a = this.commandRegistry).executeCommand.apply(_a, __spread([command.id], command.arguments ? command.arguments : []));
        }
    };
    TimelineTreeWidget.prototype.handleLeft = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.model.selectPrevNode();
                return [2 /*return*/];
            });
        });
    };
    var TimelineTreeWidget_1;
    TimelineTreeWidget.ID = 'timeline-tree-widget';
    TimelineTreeWidget.PAGE_SIZE = 20;
    __decorate([
        inversify_1.inject(common_1.MenuModelRegistry),
        __metadata("design:type", common_1.MenuModelRegistry)
    ], TimelineTreeWidget.prototype, "menus", void 0);
    __decorate([
        inversify_1.inject(timeline_context_key_service_1.TimelineContextKeyService),
        __metadata("design:type", timeline_context_key_service_1.TimelineContextKeyService)
    ], TimelineTreeWidget.prototype, "contextKeys", void 0);
    TimelineTreeWidget = TimelineTreeWidget_1 = __decorate([
        inversify_1.injectable(),
        __param(0, inversify_1.inject(tree_1.TreeProps)),
        __param(1, inversify_1.inject(timeline_service_1.TimelineService)),
        __param(2, inversify_1.inject(timeline_tree_model_1.TimelineTreeModel)),
        __param(3, inversify_1.inject(browser_1.ContextMenuRenderer)),
        __param(4, inversify_1.inject(common_1.CommandRegistry)),
        __metadata("design:paramtypes", [Object, timeline_service_1.TimelineService,
            timeline_tree_model_1.TimelineTreeModel,
            browser_1.ContextMenuRenderer,
            common_1.CommandRegistry])
    ], TimelineTreeWidget);
    return TimelineTreeWidget;
}(tree_1.TreeWidget));
exports.TimelineTreeWidget = TimelineTreeWidget;
var TimelineItemNode = /** @class */ (function (_super) {
    __extends(TimelineItemNode, _super);
    function TimelineItemNode() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.open = function () {
            var _a;
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            var command = _this.props.timelineItem.command;
            if (command) {
                (_a = _this.props.commandRegistry).executeCommand.apply(_a, __spread([command.id], command.arguments ? command.arguments : []));
            }
        };
        _this.renderContextMenu = function (event) {
            event.preventDefault();
            event.stopPropagation();
            var _a = _this.props, timelineItem = _a.timelineItem, contextKeys = _a.contextKeys, contextMenuRenderer = _a.contextMenuRenderer;
            var currentTimelineItem = contextKeys.timelineItem.get();
            contextKeys.timelineItem.set(timelineItem.contextValue);
            try {
                contextMenuRenderer.render({
                    menuPath: exports.TIMELINE_ITEM_CONTEXT_MENU,
                    anchor: event.nativeEvent,
                    args: [timelineItem]
                });
            }
            finally {
                contextKeys.timelineItem.set(currentTimelineItem);
            }
        };
        return _this;
    }
    TimelineItemNode.prototype.render = function () {
        var _a = this.props.timelineItem, label = _a.label, description = _a.description, detail = _a.detail;
        return React.createElement("div", { className: 'timeline-item', title: detail, onContextMenu: this.renderContextMenu, onClick: this.open },
            React.createElement("div", { className: "noWrapInfo " + tree_1.TREE_NODE_SEGMENT_GROW_CLASS },
                React.createElement("span", { className: 'name' }, label),
                React.createElement("span", { className: 'label' }, description)));
    };
    return TimelineItemNode;
}(React.Component));
exports.TimelineItemNode = TimelineItemNode;


/***/ }),

/***/ "./node_modules/@theia/timeline/lib/browser/timeline-widget.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@theia/timeline/lib/browser/timeline-widget.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/********************************************************************************
 * Copyright (C) 2020 RedHat and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimelineWidget = void 0;
var inversify_1 = __webpack_require__(/*! inversify */ "./node_modules/inversify/lib/inversify.js");
var browser_1 = __webpack_require__(/*! @theia/core/lib/browser */ "./node_modules/@theia/core/lib/browser/index.js");
var timeline_tree_widget_1 = __webpack_require__(/*! ./timeline-tree-widget */ "./node_modules/@theia/timeline/lib/browser/timeline-tree-widget.js");
var timeline_service_1 = __webpack_require__(/*! ./timeline-service */ "./node_modules/@theia/timeline/lib/browser/timeline-service.js");
var common_1 = __webpack_require__(/*! @theia/core/lib/common */ "./node_modules/@theia/core/lib/common/index.js");
var timeline_empty_widget_1 = __webpack_require__(/*! ./timeline-empty-widget */ "./node_modules/@theia/timeline/lib/browser/timeline-empty-widget.js");
var algorithm_1 = __webpack_require__(/*! @phosphor/algorithm */ "./node_modules/@phosphor/algorithm/lib/index.js");
var vscode_uri_1 = __webpack_require__(/*! vscode-uri */ "./node_modules/vscode-uri/lib/esm/index.js");
var timeline_service_2 = __webpack_require__(/*! ./timeline-service */ "./node_modules/@theia/timeline/lib/browser/timeline-service.js");
var TimelineWidget = /** @class */ (function (_super) {
    __extends(TimelineWidget, _super);
    function TimelineWidget() {
        var _this = _super.call(this) || this;
        _this.timelinesBySource = new Map();
        _this.id = TimelineWidget_1.ID;
        _this.title.label = 'Timeline';
        _this.title.caption = _this.title.label;
        _this.addClass('theia-timeline');
        return _this;
    }
    TimelineWidget_1 = TimelineWidget;
    TimelineWidget.prototype.init = function () {
        var _this = this;
        var layout = new browser_1.PanelLayout();
        this.layout = layout;
        this.panel = new browser_1.Panel({ layout: new browser_1.PanelLayout({}) });
        this.panel.node.tabIndex = -1;
        layout.addWidget(this.panel);
        this.containerLayout.addWidget(this.resourceWidget);
        this.containerLayout.addWidget(this.timelineEmptyWidget);
        this.refresh();
        this.toDispose.push(this.timelineService.onDidChangeTimeline(function (event) {
            var currentWidgetUri = _this.getCurrentWidgetUri();
            if (currentWidgetUri) {
                _this.loadTimeline(currentWidgetUri, event.reset);
            }
        }));
        this.toDispose.push(this.selectionService.onSelectionChanged(function (selection) {
            if (Array.isArray(selection) && 'uri' in selection[0]) {
                _this.refresh(selection[0].uri);
            }
        }));
        this.toDispose.push(this.applicationShell.onDidChangeCurrentWidget(function (e) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if ((e.newValue && browser_1.Navigatable.is(e.newValue)) || !this.suitableWidgetsOpened()) {
                    this.refresh();
                }
                return [2 /*return*/];
            });
        }); }));
        this.toDispose.push(this.applicationShell.onDidRemoveWidget(function (widget) {
            if (browser_1.NavigatableWidget.is(widget)) {
                _this.refresh();
            }
        }));
        this.toDispose.push(this.timelineService.onDidChangeProviders(function () { return _this.refresh(); }));
    };
    TimelineWidget.prototype.loadTimelineForSource = function (source, uri, reset) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var timeline, cursor, options, timelineResult, items;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (reset) {
                            this.timelinesBySource.delete(source);
                        }
                        timeline = this.timelinesBySource.get(source);
                        cursor = timeline === null || timeline === void 0 ? void 0 : timeline.cursor;
                        options = { cursor: reset ? undefined : cursor, limit: timeline_tree_widget_1.TimelineTreeWidget.PAGE_SIZE };
                        return [4 /*yield*/, this.timelineService.getTimeline(source, uri, options, { cacheResults: true, resetCache: reset })];
                    case 1:
                        timelineResult = _b.sent();
                        if (timelineResult) {
                            items = timelineResult.items;
                            if (items) {
                                if (timeline) {
                                    timeline.add(items);
                                    timeline.cursor = (_a = timelineResult.paging) === null || _a === void 0 ? void 0 : _a.cursor;
                                }
                                else {
                                    timeline = new timeline_service_2.TimelineAggregate(timelineResult);
                                }
                                this.timelinesBySource.set(source, timeline);
                                this.resourceWidget.model.updateTree(timeline.items, !!timeline.cursor);
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    TimelineWidget.prototype.loadTimeline = function (uri, reset) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, source;
            var e_1, _c;
            return __generator(this, function (_d) {
                try {
                    for (_a = __values(this.timelineService.getSources().map(function (s) { return s.id; })), _b = _a.next(); !_b.done; _b = _a.next()) {
                        source = _b.value;
                        this.loadTimelineForSource(source, vscode_uri_1.URI.parse(uri.toString()), reset);
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                return [2 /*return*/];
            });
        });
    };
    TimelineWidget.prototype.refresh = function (uri) {
        if (!uri) {
            uri = this.getCurrentWidgetUri();
        }
        if (uri) {
            this.timelineEmptyWidget.hide();
            this.resourceWidget.show();
            this.loadTimeline(uri, true);
        }
        else if (!this.suitableWidgetsOpened()) {
            this.timelineEmptyWidget.show();
            this.resourceWidget.hide();
        }
    };
    TimelineWidget.prototype.suitableWidgetsOpened = function () {
        var _this = this;
        return !!algorithm_1.toArray(this.applicationShell.mainPanel.widgets()).find(function (widget) {
            if (browser_1.NavigatableWidget.is(widget)) {
                var uri = widget.getResourceUri();
                if ((uri === null || uri === void 0 ? void 0 : uri.scheme) && _this.timelineService.getSchemas().indexOf(uri === null || uri === void 0 ? void 0 : uri.scheme) > -1) {
                    return true;
                }
            }
        });
    };
    TimelineWidget.prototype.getCurrentWidgetUri = function () {
        var current = this.applicationShell.currentWidget;
        if (!browser_1.NavigatableWidget.is(current)) {
            current = algorithm_1.toArray(this.applicationShell.mainPanel.widgets()).find(function (widget) {
                if (widget.isVisible && !widget.isHidden) {
                    return widget;
                }
            });
        }
        return browser_1.NavigatableWidget.is(current) ? current.getResourceUri() : undefined;
    };
    Object.defineProperty(TimelineWidget.prototype, "containerLayout", {
        get: function () {
            return this.panel.layout;
        },
        enumerable: false,
        configurable: true
    });
    TimelineWidget.prototype.onUpdateRequest = function (msg) {
        browser_1.MessageLoop.sendMessage(this.resourceWidget, msg);
        browser_1.MessageLoop.sendMessage(this.timelineEmptyWidget, msg);
        this.refresh();
        _super.prototype.onUpdateRequest.call(this, msg);
    };
    TimelineWidget.prototype.onAfterAttach = function (msg) {
        this.node.appendChild(this.resourceWidget.node);
        this.node.appendChild(this.timelineEmptyWidget.node);
        _super.prototype.onAfterAttach.call(this, msg);
        this.update();
    };
    var TimelineWidget_1;
    TimelineWidget.ID = 'timeline-view';
    __decorate([
        inversify_1.inject(timeline_tree_widget_1.TimelineTreeWidget),
        __metadata("design:type", timeline_tree_widget_1.TimelineTreeWidget)
    ], TimelineWidget.prototype, "resourceWidget", void 0);
    __decorate([
        inversify_1.inject(timeline_service_1.TimelineService),
        __metadata("design:type", timeline_service_1.TimelineService)
    ], TimelineWidget.prototype, "timelineService", void 0);
    __decorate([
        inversify_1.inject(common_1.CommandRegistry),
        __metadata("design:type", common_1.CommandRegistry)
    ], TimelineWidget.prototype, "commandRegistry", void 0);
    __decorate([
        inversify_1.inject(browser_1.ApplicationShell),
        __metadata("design:type", browser_1.ApplicationShell)
    ], TimelineWidget.prototype, "applicationShell", void 0);
    __decorate([
        inversify_1.inject(timeline_empty_widget_1.TimelineEmptyWidget),
        __metadata("design:type", timeline_empty_widget_1.TimelineEmptyWidget)
    ], TimelineWidget.prototype, "timelineEmptyWidget", void 0);
    __decorate([
        inversify_1.inject(common_1.SelectionService),
        __metadata("design:type", common_1.SelectionService)
    ], TimelineWidget.prototype, "selectionService", void 0);
    __decorate([
        inversify_1.postConstruct(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TimelineWidget.prototype, "init", null);
    TimelineWidget = TimelineWidget_1 = __decorate([
        inversify_1.injectable(),
        __metadata("design:paramtypes", [])
    ], TimelineWidget);
    return TimelineWidget;
}(browser_1.BaseWidget));
exports.TimelineWidget = TimelineWidget;


/***/ })

}]);
//# sourceMappingURL=30.bundle.js.map